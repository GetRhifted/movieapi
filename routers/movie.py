from fastapi import APIRouter
from fastapi import Path, Query, Depends
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List
from config.database import Session
from models.movie import Movie as MovieModel
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBearer
from services.movie import MovieService
from schemas.movie import Movie

movie_router = APIRouter()


@movie_router.get('/Movies', tags=['Movies'], status_code=200)
def get_movies():
    db = Session()
    result = MovieService(db).get_movies()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@movie_router.get('/Movies/{id}', tags=['Movies'], status_code=200, dependencies=[Depends(JWTBearer())])
def get_movie(id: int = Path(ge=1, le=2000)):
    message = 'Parece que te has equivocado, prueba de nuevo!'
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content=message)
    return JSONResponse(status_code=200, content=jsonable_encoder(result)) 

@movie_router.get('/Movies/', tags=['Movies'], dependencies=[Depends(JWTBearer())])
def get_movies_by_category(category: str = Query(min_length=5, max_length=15)):
    db = Session()
    result = MovieService(db).get_movie_category(category)
    if not result:
        return JSONResponse(status_code=404, content={'message', 'Parece que la categoria no se encuentra en nuestra base de datos.'})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@movie_router.post('/Movies', tags=['Movies'], status_code=201, dependencies=[Depends(JWTBearer())])
def create_movie(movie: Movie):
    db = Session()
    MovieService(db).create_movie(movie)
    return JSONResponse(status_code=201, content={'message': 'Se ha agregado la pelicula!'})

@movie_router.put('/Movies/{id}', tags=['Movies'], status_code=200, dependencies=[Depends(JWTBearer())])
def update_movie(id : int, movie: Movie):
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={'message' : 'Parece que te has equivocado, prueba de nuevo!'})
    MovieService(db).update_movie(id, movie)
    db.commit()
    return JSONResponse(status_code=200, content={'message': 'Se ha actualizado la pelicula!'})

@movie_router.delete('/Movies/{id}', tags=['Movies'], status_code=200, dependencies=[Depends(JWTBearer())])
def delete_movie(id: int):
    db = Session()
    result = MovieService(db).get_movie(id)    
    if not result:
        return JSONResponse(status_code=404, content={'message' : 'Parece que te has equivocado, prueba de nuevo!'})
    MovieService(db).delete_movie(id)
    return JSONResponse(status_code=200, content={'message' : 'Se ha eliminado la pelicula!'})
