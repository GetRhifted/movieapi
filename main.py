from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from pydantic import BaseModel
from config.database import engine, Base
from middlewares.error_handler import ErrorHandler
from routers.movie import movie_router
from routers.user import user_router

app = FastAPI()
app.title = 'Mi Aplicacion con FastAPI'
app.version = '0.0.1'

app.add_middleware(ErrorHandler)
app.include_router(user_router)
app.include_router(movie_router)

Base.metadata.create_all(bind=engine)

movies = [
    {
        'id': 1,
        'title': 'Avatar',
        'overview': "En un exuberante planeta llamado Pandora viven los Na'vi, seres que ...",
        'year': '2009',
        'rating': 7.8,
        'category': 'Acción'
    },
    {      
        'id': 2,
        'title': 'Whiplash',
        'overview': "Andrew Neiman es un joven y ambicioso baterista de jazz. Marcado por el fracaso de la carrera literaria de su padre, está obsesionado con...",
        'year': '2014',
        'rating': 7.8,
        'category': 'Drama, Musica'  
    },
    {
        'id': 3,
        'title': 'Taxi Driver',
        'overview': "Un veterano de Vietnam inicia una confrontación violenta con los proxenetas que trabajan en las calles de Nueva York.",
        'year': '1976',
        'rating': 8.1,
        'category': 'Drama'
    }
]

@app.get('/', tags=['Home'])
def message():
    return HTMLResponse('<h1>Hello World</h1>')
